This is a port of the DLI/Course for Accelerating Applications with GPU
Accelerated Libraries in C/C++.<br/>
<https://courses.nvidia.com/courses/course-v1:DLI+L-AC-17+V1/about>

Follow along with the instructions below or via python notebook
[GPU_Libraries_C.ipynb](src/GPU_Libraries_C.ipynb)

# Accelerating C/C++ code with Libraries on GPUs

In this self-paced, hands-on lab, we will use libraries to accelerate code on
NVIDIA GPUs.

Lab created by Mark Ebersole (Follow [@CUDAHamster](https://twitter.com/@cudahamster)
on Twitter)

## Introduction to GPU Libraries

GPU-accelerated libraries are a great and easy way to quickly speed-up the
computationally intensive portions of your code.  You are able to access
highly-tuned and GPU optimized algorithms without having to write any of that code.

This lab consists of three tasks that will require you to modify, compile and
execute some code.  For each task, a solution is provided so you can check your
work or take a peek if you get lost.

If you are still confused now, or at any point in this lab, you can consult the
[FAQ](#FAQ) located at the bottom of this page.

### Task #1

For this first task, we'll be using the [cuBLAS](https://developer.nvidia.com/cuBLAS)
GPU-accelerated library to do a basic matrix multiply. The specific API we'll
be using is `cublasSgemm` (the `S` stands for `single` and the `gemm` for
**GE**neral **M**atrix-**M**atrix Multiply) and you'll want to use the
documentation for this call located at [docs.nvidia.com](http://docs.nvidia.com/cuda/cublas/index.html#cublas-lt-t-gt-gemm).

#### GPU Memory
It is important to realize that the GPU has its own physical memory just like
the CPU uses system RAM. When executing code on the GPU, we have to ensure any
data it needs is first copied across the PCI-Express bus to the GPU's memory
before we call a library API.

Starting in CUDA 6 and with the Kepler architecture, Unified Memory replaced
[manual handling of data movement.](http://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__MEMORY.html#group__CUDART__MEMORY)
Unified Memory creates an area of managed memory, and the underlying system
handles moving this managed data between CPU and GPU memory spaces when
required. Because of this, getting an application executing on an NVIDIA GPU
with CUDA C/C++ has become a much quicker and more efficient process - including
simpler to maintain code.

To make use of this managed memory area with Unified Memory, you use the
following API calls.

* `cudaMallocManaged ( T** devPtr, size_t size );` - allocate `size` bytes in
managed memory and store in devPtr.
* `cudaFree ( void* devPtr );` - we use this API call to free any memory we
allocated in managed memory.

Once you have used `cudaMallocManaged` to allocate some data, you just use the
pointer in your code, regardless of whether it's the CPU or GPU accessing the data.

Managed memory is synchronized between memory spaces at kernel launch and any
device synchronization points.  Please visit the CUDA documentation page on
[Unified Memory](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#um-unified-memory-programming-hd)
to read about it in more detail.

In the code below, there is a single-threaded CPU version of matrix multiply.
We'll compare the CPU version with a GPU library call.

---

#### **Exercise** 

Replace the `## FIXME: ... ##` regions of code to successfully call
`cublasSgemm` and do a basic matrix multiply in the file
[task1.cu](src/task1-sgemm/task1-orig.cu).

If you get stuck, there are a number of hints provided below and a solution in
the file [task1_solution.cu](src/task1-sgemm/task1_solution.cu).
Feel free to compare your work or use it if you are lost.

When you are ready to compile and run, there are two executable cells below.
The first one compiles task1.cu, and the second runs the resulting program.
Remember you execute these cells with Ctrl-Enter, or by pressing the Play
button in the toolbar at the top of the window.

<a name='task1'></a>
[Hint #1](#task1hint1)<br/>
[Hint #2](#task1hint2)<br/>
[Hint #3](#task1hint3)

```bash
# Execute this cell to compile ttask1
nvcc -arch=sm_30 -lcublas -o task1_out task1-sgemm/task1.cu && echo Compiled Successfully!
```

```bash
./task1_out
```

With just a few lines of code, we were able to call a highly optimized version
of matrix-multiply, instead of having to write that code ourselves.  And with
each new generation of GPUs, the libraries will be updated to continually take
advantage of enhanced performance and features.

In the above task, we only used a single library call.  However, the real power
of libraries tends to come from stringing multiple calls together to build a more
in-depth algorithm.  We'll explore using multiple GPU libraries in the next task.

### Task #2

For this task, we're going to make use of the GPU to create the random numbers
used in our matrix multiplication.  While this is a contrived example, it's a
good simplistic way to show the concepts of using multiple GPU libraries in tandem.

The CPU-side matrix multiply has been removed, and we'll be using just the
cublasSgemm to do the multiply.

---

#### Exercise

Your objective in this task is replace the for-loop and rand() calls with two
host-side version of the [cuRAND](https://developer.nvidia.com/cuRAND) `curandGenerateNormal`
API call.  You'll want to look at the cuRAND
[documentation](http://docs.nvidia.com/cuda/curand/host-api-overview.html#generation-functions)
for this to see what you need to do.  

**Before** you modify any code, you should compile and execute
[task2.cu](src/task2-curand/task2-orig.cu) using the two cells below. This will give
you a benchmark on how long the original code takes to run.

Again, we have included some hints and a
[solution](src/task2-curand/task2_solution.cu).

<a name='task2'></a>
[Hint #1](#task2hint1)<br/>
[Hint #2](#task2hint2)

```bash
# Execute this cell to compile task2.cu
nvcc -arch=sm_30 -lcublas -lcurand -o task2_out task2-curand/task2.cu && echo Compiled Successfully!
```

```bash
./task2_out
```

If you have successfully used the cuRAND library to create random data for
matrices `a` and `b`, and used the `curandCreateGeneratorHost` call, you will
probably have noticed that the whole program may take **longer** to run than
the original.  That's definitely not what we want or expect when moving
computation to the GPU.  So what gives?

This is a great opportunity to demonstrate the command-line profile provided in
the [CUDA Toolkit](https://developer.nvidia.com/cuda-toolkit) from NVIDIA; nvprof.
Execute the below cell to profile your task2_out program and we'll see if we
can figure out what the problem is. This is called profiler-driven optimization.


```bash
nvprof ./task2_out
```

Here, you can see that with regards to *API calls,* cudaFree and
cudaMallocManaged consume the vast majority of time. While unified memory
*handles* transfers between the host (CPU) and device (GPU), those tranfers
still take time. The *Unified Memory profiling result* shows the total amount
of time spent migrating that memory. 

If your modified [task2.cu](src/task2-curand//task2.cu)
file is similar to the [task2_solution.cu](src/task2-curand/task2_solution.cu)
code, the nvprof will show exactly how much time is spent migrating the data to
the device. Here is a clipped output of nvprof for the task 2 solution (exact
details may differ depending on your code and which GPU architecture you are using):

```
==2458== NVPROF is profiling process 2458, command: ./task2_out

API calls:
81.54%  759.86ms         7  108.55ms  88.919us  360.22ms  cudaFree
18.30%  170.55ms         3  56.850ms  26.303us  170.46ms  cudaMallocManaged
0.05%   501.29us         2  250.65us  248.55us  252.74us  cuDeviceTotalMem
0.04%   419.23us       185  2.2660us     263ns  71.632us  cuDeviceGetAttribute
0.03%   296.56us         3  98.852us  11.804us  161.31us  cudaMalloc

==2458== Unified Memory profiling result:
Device "Tesla V100-SXM2-16GB (0)"
Count  Avg Size  Min Size  Max Size  Total Size  Total Time  Name
8452  46.151KB  4.0000KB  0.9922MB  380.9336MB  57.74902ms  Host To Device
742          -         -         -           -  197.7161ms  Gpu page fault groups
Total CPU Page faults: 2292
```

If we can reduce the number of data transfers, we can increase performance. It
turns out we have excessive data transfers because we used the host-side
version of `curandCreateGeneratorHost`. So what's currently happening is:

1. The CPU creates random numbers
2. These numbers migrate to the GPU to be used by our `cublasSgemm` call.

So we're doing 2 transfers (2x arrays, 1 time each) of data that aren't required!

### Task #3

Your final task is to modify [task3.cu](src/task3-curand_on_gpu/task3-orig.cu)
(the same as [task2_solution.cu](src/task2-curand/task2_solution.cu)
in Task #2) to minimize the number of transfers across the PCI-Express bus.
When complete, make sure to save and then run in the cell below. 

Make use of the cuRAND [documentation](http://docs.nvidia.com/cuda/curand/host-api-overview.html#generation-functions),
the following hint, or the [solution](src/task3-curand_on_gpu/task3_solution.cu)
if needed.

<a name='task3'></a>
[Hint 1](#task3hint1)

```bash
# Execute this cell to compile task3.cu
nvcc -arch=sm_30 -lcublas -lcurand -o task3_out task3/task3.cu && echo Compiled Successfully!
```


```bash
./task3_out
```

In my case, with the modification to the random number generation, our
contrived example is now running about three times as fast as we started
with - and we did not have to write any GPU specific code, just make a few
library API calls.

You can now see how leaving data on the GPU for as long as possible, and
stringing together API calls from various libraries, can provide great
flexibility in creating extremely fast and powerful algorithms!

## Learn More

If you are interested in learning more, you can use the following resources:

* Learn more at the [CUDA Developer Zone](https://developer.nvidia.com/category/zone/cuda-zone).
* See a list of available libraries [here](https://developer.nvidia.com/gpu-accelerated-libraries).
* If you have an NVIDIA GPU in your system, you can download and install the
[CUDA tookit](https://developer.nvidia.com/cuda-toolkit) to access NVIDIA's
GPU-accelerated libraries.
* Search or ask questions on [Stackoverflow](http://stackoverflow.com/questions/tagged/cuda)
using the cuda tag.

---
# <a name="FAQ"></a> Lab FAQ

Q: I'm encountering issues executing the cells, or other technical problems?<br>
A: Please see [this](https://developer.nvidia.com/self-paced-labs-faq#Troubleshooting) infrastructure FAQ.

# Lab Hints

## <a name="task1hint1"></a> Task 1 - Hint 1

The *cublasHandle_t* required by the *cublasSgemm* call has already been
created for you in the code.

[Return to task](#task1)

## <a name='task1hint2'></a> Task 1 - Hint 2

You will need to use *cudaMallocManaged* to  allocate space for the *a*, *b*,
and *c* arrays in unified memory.  These pointers have already been declared for you.

[Return to task](#task1)

## <a name='task1hint3'></a> Task 1 - Hint 3

You'll need to use the [cublasSgemm documentation](http://docs.nvidia.com/cuda/cublas/index.html#cublas-lt-t-gt-gemm)
to determine the remaining parameters needed in the call.

[Return to task](#task1)

## <a name='task2hint1'></a> Task 2 - Hint 1

For this task, make sure to use the Host version of the
*curandCreateGeneratorHost* call.  Well explore the device-side version in the
next task.

[Return to task](#task2)

## <a name='task2hint2'></a> Task 2 - Hint 2
There's a great example showing how to use the cuRAND library located
[here](http://docs.nvidia.com/cuda/curand/host-api-overview.html#host-api-example).

[Return to task](#task2)

## <a name='task3hint1'></a> Task 3 - Hint 1

To have the *cuRAND* library fill a device-side array, you'll want to use
*curandCreateGenerator* without the *Host* on the end.

[Return to task](#task3)

#include "cuda_runtime.h"
#include "cublas_v2.h"
#include "curand.h"
#include <stdio.h>

#include "../../src/task3/timer.h"

#define INDX( row, col, ld ) ( ( (col) * (ld) ) + (row) )

#define SIZE 10000

int main()
{
    const int size = SIZE;

    fprintf(stdout, "Matrix size is %d\n",size);

    float *a, *b, *c;
 
    size_t numbytes = size * size * sizeof( float );

    // Allocate all our host-side (CPU) and device-side (GPU) data
    cudaMallocManaged( (void **)&a, numbytes);
    cudaMallocManaged( (void **)&b, numbytes);
    cudaMallocManaged( (void **)&c, numbytes);

    if( a == NULL || b == NULL || c == NULL)
    {
      fprintf(stderr,"Error in malloc\n");
      return 911;
    }

    cublasHandle_t handle;
    cublasStatus_t stat = cublasCreate( &handle );

    // Set these constants so we get a simple matrix multiply with cublasDgemm
    float alpha = 1.0;
    float beta  = 0.0;
  
    StartTimer();

    // Generate size * size random numbers
    printf("Create random numbers\n");
    // Create pseudo-random number generator
    curandGenerator_t gen;
    curandCreateGeneratorHost(&gen, CURAND_RNG_PSEUDO_DEFAULT);

    curandGenerateNormal(gen, a, size*size, 0.0, float(RAND_MAX));
    curandGenerateNormal(gen, b, size*size, 0.0, float(RAND_MAX));

    // Launch cublasDgemm on the GPU
    printf("Launching GPU dgemm\n");
    cublasSgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N,
                 size, size, size,
                 &alpha, 
                 a, size,
                 b, size,
                 &beta,
                 c, size );

    double runtime = GetTimer();

    fprintf(stdout, "Total time is %f sec\n", runtime / 1000.0f );

    cublasDestroy( handle );
    curandDestroyGenerator( gen );

    cudaFree( a );
    cudaFree( b );
    cudaFree( c );

    return 0;
}

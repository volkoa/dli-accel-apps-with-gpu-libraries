/*
 * Compile: nvcc -arch=sm_30 -lcublas -o task1_out task1/task1.cu && echo Compiled Successfully!
 */
#include "cuda_runtime.h"
#include "cublas_v2.h"
#include <stdio.h>
#include <assert.h>
#include "timer.h"

inline cudaError_t checkCuda(cudaError_t result) {
    if (result != cudaSuccess) {
        fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
        assert(result == cudaSuccess);
    }
    return result;
}

#define INDX( row, col, ld ) ( ( (col) * (ld) ) + (row) )

#define SIZE 1024

// A single-threaded version of matrix multiply
void host_sgemm(int m, int n, int k, float *a, float *b, float *c) {
    for (int j = 0; j < n; j++) {
        for (int i = 0; i < m; i++) {
            for (int koff = 0; koff < k; koff++) {
                c[INDX(i, j, m)] += a[INDX(i, koff, m)] * b[INDX(koff, j, n)];
            } /* end for i */
        } /* end jb */
    } /* end for j */
} /* end host_sgemm */

int main() {
    const int size = SIZE;

    fprintf(stdout, "Matrix size is %d\n", size);

    float *a, *b, *c, *cdef;

    size_t numbytes = size * size * sizeof(float);

    // Allocate our host-side (CPU) AND device-side (GPU) memory
    // ## FIXME: Allocate a, b, and c arrays in Unified memory ##
    /* h_a = (float *) malloc( numbytes );
     if( a == NULL || b == NULL || c == NULL || cdef == NULL )
     {
     fprintf(stderr,"Error in host malloc\n");
     return 911;
     } */
    checkCuda(cudaMallocManaged((void **) &a, numbytes));
    checkCuda(cudaMallocManaged((void **) &b, numbytes));
    checkCuda(cudaMallocManaged((void **) &c, numbytes));
    checkCuda(cudaMallocManaged((void **) &cdef, numbytes));

    // Clear the result matrices to zero
    memset(c, 0, numbytes);
    memset(cdef, 0, numbytes);

    // Initialize the a and b matrices to random data
    for (int i = 0; i < size * size; i++) {
        a[i] = float(rand()) / (float(RAND_MAX) + 1.0);
        b[i] = float(rand()) / (float(RAND_MAX) + 1.0);
    }

    // First run the CPU verison of sgemm so we can compare the results
    StartTimer();

    printf("Launching CPU sgemm\n");
    host_sgemm(size, size, size, a, b, cdef);

    double runtime = GetTimer();

    fprintf(stdout, "Total time CPU is %f sec\n", runtime / 1000.0f);
    fprintf(stdout, "Performance is %f GFlop/s\n",
        2.0 * (double) size * (double) size * (double) size
            / ((double) runtime / 1000.0) * 1.e-9);

    // Now run the GPU version of sgemm using the cuBLAS library
    cublasHandle_t handle;
    cublasStatus_t stat = cublasCreate(&handle);

    // Set these constants so we get a simple matrix multiply with cublasSgemm
    float alpha = 1.0;
    float beta = 0.0;

    StartTimer();

    // Launch cublasSgemm on the GPU
    printf("Launching GPU sgemm\n");
    //cublasSgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N,
    //## FIXME: Fill out the rest of the parameters ##
    //);

    /*  Use docs link and API documentation.
     https://docs.nvidia.com/cuda/cublas/index.html#cublas-lt-t-gt-gemm
     cublasStatus_t cublasSgemm(cublasHandle_t handle,
         cublasOperation_t transa, cublasOperation_t transb,
         int m, int n, int k,
         const float           *alpha,
         const float           *A, int lda,
         const float           *B, int ldb,
         const float           *beta,
         float           *C, int ldc);
     */

    cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N,
        size, size, size,
        &alpha,
        a, size,
        b, size,
        &beta,
        c, size);

    runtime = GetTimer();

    fprintf(stdout, "Total time GPU CUBLAS is %f sec\n", runtime / 1000.0f);
    fprintf(stdout, "Performance is %f GFlop/s\n",
        2.0 * (double) size * (double) size * (double) size
            / ((double) runtime / 1000.0) * 1.e-9);

    cublasDestroy(handle);

    // Do some error checking to verify our GPU & CPU verisons are within
    // an acceptable error bound
    float temp = 0.0;
    for (int i = 0; i < size * size; i++) {
        temp += (c[i] - cdef[i]) * (c[i] - cdef[i]);
    } /* end for */

    printf("error is %f\n", temp);
    if (temp > 10)
        printf("Error value is suspiciously high!\n");

    cudaFree(a);
    cudaFree(b);
    cudaFree(c);
    cudaFree(cdef);

    return 0;
}

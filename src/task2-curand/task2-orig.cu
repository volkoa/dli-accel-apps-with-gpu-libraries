/*
 * nvcc -arch=sm_30 -lcublas -lcurand -o task2_orig_out task2/task2-orig.cu && echo Compiled Successfully!
 */
#include "cuda_runtime.h"
#include "cublas_v2.h"
#include "curand.h"
#include <stdio.h>
#include "timer.h"

#define INDX( row, col, ld ) ( ( (col) * (ld) ) + (row) )

#define SIZE 10000

int main()
{
    const int size = SIZE;

    fprintf(stdout, "Matrix size is %d\n",size);

    float *a, *b, *c;
 
    size_t numbytes = size * size * sizeof( float );

    // Allocate all our host-side (CPU) and device-side (GPU) data
    cudaMallocManaged( (void **)&a, numbytes);
    cudaMallocManaged( (void **)&b, numbytes);
    cudaMallocManaged( (void **)&c, numbytes);

    if( a == NULL || b == NULL || c == NULL)
    {
      fprintf(stderr,"Error in malloc\n");
      return 911;
    }

    cublasHandle_t handle;
    cublasStatus_t stat = cublasCreate( &handle );

    // Set these constants so we get a simple matrix multiply with cublasSgemm
    float alpha = 1.0;
    float beta  = 0.0;
  
    StartTimer();

    // Generate size * size random numbers
    printf("Create random numbers\n");
    // FIXME: Replace the following for-loop with two curandGenerateNormal calls
    for( int i = 0; i < size * size; i++ )
    {
      a[i] = float( rand() ) / ( float(RAND_MAX) + 1.0 );
      b[i] = float( rand() ) / ( float(RAND_MAX) + 1.0 );
    }

    // Launch cublasSgemm on the GPU
    printf("Launching GPU sgemm\n");
    cublasSgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N,
                 size, size, size,
                 &alpha, 
                 a, size,
                 b, size,
                 &beta,
                 c, size );

    double runtime = GetTimer();

    fprintf(stdout, "Total time is %f sec\n", runtime / 1000.0f );

    cublasDestroy( handle );

    cudaFree( a );
    cudaFree( b );
    cudaFree( c );

    return 0;
}

/*
 * nvcc -arch=sm_30 -lcublas -lcurand -o task2_out task2/task2.cu && echo Compiled Successfully!
 * Profile:
 *     nvprof ./task2_out
 * Profile example output:
 * $ nvprof ./task2_out
Matrix size is 10000
==17320== NVPROF is profiling process 17320, command: ./task2_out
Create random numbers
Launching GPU sgemm
Total time is 12.328691 sec
==17320== Profiling application: ./task2_out
==17320== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:  100.00%  696.98ms         1  696.98ms  696.98ms  696.98ms  volta_sgemm_128x128_nn
                    0.00%  1.7600us         1  1.7600us  1.7600us  1.7600us  [CUDA memcpy HtoD]
      API calls:   85.38%  1.27212s         7  181.73ms  40.566us  697.38ms  cudaFree
                   14.23%  212.05ms         3  70.683ms  21.214us  211.99ms  cudaMallocManaged
                    0.16%  2.3123ms         4  578.08us  567.12us  590.03us  cuDeviceTotalMem
                    0.14%  2.0482ms       378  5.4180us     134ns  242.50us  cuDeviceGetAttribute
                    0.04%  602.73us         1  602.73us  602.73us  602.73us  cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
                    0.02%  268.00us         1  268.00us  268.00us  268.00us  cudaLaunchKernel
                    0.02%  246.92us         3  82.306us  4.8610us  231.67us  cudaMalloc
                    0.01%  207.50us         4  51.874us  48.080us  55.616us  cuDeviceGetName
                    0.00%  60.037us        76     789ns     421ns  8.0260us  cudaFuncSetAttribute
                    0.00%  28.836us         1  28.836us  28.836us  28.836us  cudaMemcpy
                    0.00%  13.984us         2  6.9920us  2.5960us  11.388us  cudaDeviceSynchronize
                    0.00%  13.942us        16     871ns     409ns  7.1590us  cudaEventCreateWithFlags
                    0.00%  10.887us        16     680ns     386ns  4.0630us  cudaEventDestroy
                    0.00%  8.5460us         2  4.2730us  1.9880us  6.5580us  cuDeviceGetPCIBusId
                    0.00%  5.7890us        11     526ns     293ns  2.1060us  cudaDeviceGetAttribute
                    0.00%  4.0260us         1  4.0260us  4.0260us  4.0260us  cudaGetDevice
                    0.00%  3.3480us         6     558ns     182ns  1.9440us  cuDeviceGet
                    0.00%  1.8490us         4     462ns     163ns  1.0120us  cuDeviceGetCount
                    0.00%  1.6370us         9     181ns     106ns     567ns  cudaGetLastError
                    0.00%     971ns         1     971ns     971ns     971ns  cuInit
                    0.00%     887ns         4     221ns     162ns     279ns  cuDeviceGetUuid
                    0.00%     713ns         1     713ns     713ns     713ns  cuDriverGetVersion

==17320== Unified Memory profiling result:
Device "Tesla T4 (0)"
   Count  Avg Size  Min Size  Max Size  Total Size  Total Time  Name
    4356  49.381KB  4.0000KB  0.9922MB  210.0625MB  27.67459ms  Host To Device
     867         -         -         -           -  184.1526ms  Gpu page fault groups
Total CPU Page faults: 2292
 */
#include "cuda_runtime.h"
#include "cublas_v2.h"
#include "curand.h"
#include <stdio.h>
#include "timer.h"

#define CUDA_CALL(x) do { if((x)!=cudaSuccess) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    return EXIT_FAILURE;}} while(0)

#define CUBLAS_CALL(x) do { if((x)!=CUBLAS_STATUS_SUCCESS) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    return EXIT_FAILURE;}} while(0)

#define CURAND_CALL(x) do { if((x)!=CURAND_STATUS_SUCCESS) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__);\
    return EXIT_FAILURE;}} while(0)

#define INDX( row, col, ld ) ( ( (col) * (ld) ) + (row) )

#define SIZE 10000

int main() {
    const int size = SIZE;

    fprintf(stdout, "Matrix size is %d\n", size);

    float *a, *b, *c;

    size_t numbytes = size * size * sizeof(float);

    // Allocate all our host-side (CPU) and device-side (GPU) data
    cudaMallocManaged((void **) &a, numbytes);
    cudaMallocManaged((void **) &b, numbytes);
    cudaMallocManaged((void **) &c, numbytes);

    if (a == NULL || b == NULL || c == NULL) {
        fprintf(stderr, "Error in malloc\n");
        return 911;
    }

    cublasHandle_t handle;
    // cublasStatus_t stat = cublasCreate(&handle);
    CUBLAS_CALL(cublasCreate(&handle));

    // Set these constants so we get a simple matrix multiply with cublasSgemm
    float alpha = 1.0;
    float beta = 0.0;

    StartTimer();

    // Generate size * size random numbers
    printf("Create random numbers\n");
    // FIXME: Replace the following for-loop with two curandGenerateNormal calls
    //for (int i = 0; i < size * size; i++) {
    //    a[i] = float(rand()) / (float(RAND_MAX) + 1.0);
    //    b[i] = float(rand()) / (float(RAND_MAX) + 1.0);
    //}
    /* Example
     * https://docs.nvidia.com/cuda/curand/host-api-overview.html#host-api-example
     * https://docs.nvidia.com/cuda/curand/host-api-overview.html#generation-functions
     * curandStatus_t  curandGenerateNormal(curandGenerator_t generator,
     *     float *outputPtr, size_t n, float mean, float stddev)
     *
     */
    curandGenerator_t gen;
    /* Create pseudo-random number generator */
    CURAND_CALL(curandCreateGeneratorHost(&gen, CURAND_RNG_PSEUDO_DEFAULT));
    /* Generate n floats on device */
    CURAND_CALL(curandGenerateNormal(gen, a, size*size, 0.0, float(RAND_MAX)));
    CURAND_CALL(curandGenerateNormal(gen, b, size*size, 0.0, float(RAND_MAX)));

    // Launch cublasSgemm on the GPU
    printf("Launching GPU sgemm\n");
    cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, size, size, size, &alpha, a,
        size, b, size, &beta, c, size);

    double runtime = GetTimer();

    fprintf(stdout, "Total time is %f sec\n", runtime / 1000.0f);

    cublasDestroy(handle);
    CURAND_CALL(curandDestroyGenerator(gen));

    cudaFree(a);
    cudaFree(b);
    cudaFree(c);

    return 0;
}

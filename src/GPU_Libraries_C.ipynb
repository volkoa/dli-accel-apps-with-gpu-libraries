{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Accelerating C/C++ code with Libraries on GPUs\n",
    "\n",
    "In this self-paced, hands-on lab, we will use libraries to accelerate code on NVIDIA GPUs.\n",
    "\n",
    "Lab created by Mark Ebersole (Follow [@CUDAHamster](https://twitter.com/@cudahamster) on Twitter)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we begin, let's verify [WebSockets](http://en.wikipedia.org/wiki/WebSocket) are working on your system.  To do this, execute the cell block below by giving it focus (clicking on it with your mouse), and hitting Ctrl-Enter, or pressing the play button in the toolbar above.  If all goes well, you should see get some output returned below the grey cell.  If not, please consult the [Self-paced Lab Troubleshooting FAQ](https://developer.nvidia.com/self-paced-labs-faq#Troubleshooting) to debug the issue. You can check whether websockets are working on your system by going to the link [http://websocketstest.com/](http://websocketstest.com/)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print \"The answer should be three: \" + str(1+2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's execute the cell below to display information about the GPUs running on the server."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "!nvidia-smi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='intro'></a> \n",
    "## Introduction to GPU Libraries\n",
    "\n",
    "GPU-accelerated libraries are a great and easy way to quickly speed-up the computationally intensive portions of your code.  You are able to access highly-tuned and GPU optimized algorithms without having to write any of that code.\n",
    "\n",
    "This lab consists of three tasks that will require you to modify, compile and execute some code.  For each task, a solution is provided so you can check your work or take a peek if you get lost.\n",
    "\n",
    "If you are still confused now, or at any point in this lab, you can consult the <a href=\"#FAQ\">FAQ</a> located at the bottom of this page."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task #1\n",
    "\n",
    "For this first task, we'll be using the [cuBLAS](https://developer.nvidia.com/cuBLAS) GPU-accelerated library to do a basic matrix multiply.  The specific API we'll be using is `cublasSgemm` (the `S` stands for `single` and the `gemm` for **GE**neral **M**atrix-**M**atrix Multiply) and you'll want to use the documentation for this call located at [docs.nvidia.com](http://docs.nvidia.com/cuda/cublas/index.html#cublas-lt-t-gt-gemm).\n",
    "\n",
    "#### GPU Memory\n",
    "It is important to realize that the GPU has its own physical memory just like the CPU uses system RAM. When executing code on the GPU, we have to ensure any data it needs is first copied across the PCI-Express bus to the GPU's memory before we call a library API.\n",
    "\n",
    "Starting in CUDA 6 and with the Kepler architecture, Unified Memory replaced [manual handling of data movement.](http://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__MEMORY.html#group__CUDART__MEMORY)  Unified Memory creates an area of managed memory, and the underlying system handles moving this managed data between CPU and GPU memory spaces when required.  Because of this, getting an application executing on an NVIDIA GPU with CUDA C/C++ has become a much quicker and more efficient process - including simpler to maintain code.\n",
    "\n",
    "To make use of this managed memory area with Unified Memory, you use the following API calls.\n",
    "\n",
    "* `cudaMallocManaged ( T** devPtr, size_t size );` - allocate `size` bytes in managed memory and store in devPtr.\n",
    "* `cudaFree ( void* devPtr );` - we use this API call to free any memory we allocated in managed memory.\n",
    "\n",
    "Once you have used `cudaMallocManaged` to allocate some data, you just use the pointer in your code, regardless of whether it's the CPU or GPU accessing the data.\n",
    "\n",
    "Managed memory is synchronized between memory spaces at kernel launch and any device synchronization points.  Please visit the CUDA documentation page on [Unified Memory](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#um-unified-memory-programming-hd) to read about it in more detail.\n",
    "\n",
    "\n",
    "\n",
    "In the code below, there is a single-threaded CPU version of matrix multiply. We'll compare the CPU version with a GPU library call.\n",
    "\n",
    "---\n",
    "\n",
    "#### **Exercise** \n",
    "\n",
    "Replace the `## FIXME: ... ##` regions of code to successfully call `cublasSgemm` and do a basic matrix multiply in the file [task1.cu](task1-sgemm/task1.cu).  \n",
    "\n",
    "After making changes, **make sure to save** using the 'file' menu.  As a reminder, saving the file actually saves it on the Amazon GPU system in the cloud you're working on.  To get a copy of the files we'll be working on, consult the <a href=\"#post-lab\">Post-Lab</a> section near the end of this page.\n",
    "\n",
    "If you get stuck, there are a number of hints provided below and a solution in the file [task1_solution](task1-sgemm/task1_solution.cu). Feel free to compare your work or use it if you are lost.\n",
    "\n",
    "When you are ready to compile and run, there are two executable cells below.  The first one compiles task1.cu, and the second runs the resulting program.  Remember you execute these cells with Ctrl-Enter, or by pressing the Play button in the toolbar at the top of the window.\n",
    "\n",
    "<a id = 'task1'></a>\n",
    "[Hint #1](#task1hint1)  \n",
    "[Hint #2](#task1hint2)  \n",
    "[Hint #3](#task1hint3)  \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Execute this cell to compile ttask1\n",
    "!nvcc -arch=sm_30 -lcublas -o task1_out task1/task1.cu && echo Compiled Successfully!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "!./task1_out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With just a few lines of code, we were able to call a highly optimized version of matrix-multiply, instead of having to write that code ourselves.  And with each new generation of GPUs, the libraries will be updated to continually take advantage of enhanced performance and features.\n",
    "\n",
    "In the above task, we only used a single library call.  However, the real power of libraries tends to come from stringing multiple calls together to build a more in-depth algorithm.  We'll explore using multiple GPU libraries in the next task."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task #2\n",
    "\n",
    "For this task, we're going to make use of the GPU to create the random numbers used in our matrix multiplication.  While this is a contrived example, it's a good simplistic way to show the concepts of using multiple GPU libraries in tandem.\n",
    "\n",
    "The CPU-side matrix multiply has been removed, and we'll just be using the cublasSgemm to do the multiply.\n",
    "\n",
    "---\n",
    "\n",
    "#### Exercise\n",
    "\n",
    "Your objective in this task is replace the for-loop and rand() calls with two host-side version of the [cuRAND](https://developer.nvidia.com/cuRAND) `curandGenerateNormal` API call.  You'll want to look at the cuRAND [documentation](http://docs.nvidia.com/cuda/curand/host-api-overview.html#generation-functions) for this to see what you need to do.  \n",
    "\n",
    "**Before** you modify any code, you should compile and execute [task2-orig.cu](task2-curand/task2-orig.cu) using the two cells below.  This will give you a benchmark on how long the original code takes to run. Modify the code in [task2.cu](task2-curand/task2.cu)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, we have included some hints and a [solution](task2-curand/task2_solution.cu).\n",
    "\n",
    "<a id = 'task2'></a>\n",
    "[Hint #1](#task2hint1)  \n",
    "[Hint #2](#task2hint2)  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Execute this cell to compile task2.cu\n",
    "!nvcc -arch=sm_30 -lcublas -lcurand -o task2_out task2/task2.cu && echo Compiled Successfully!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "!./task2_out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you have successfully used the cuRAND library to create random data for matrices `a` and `b`, and used the `curandCreateGeneratorHost` call, you will probably have noticed that the whole program may take **longer** to run than the original.  That's definitely not what we want or expect when moving computation to the GPU.  So what gives?\n",
    "\n",
    "This is a great opportunity to demonstrate the command-line profile provided in the [CUDA Toolkit](https://developer.nvidia.com/cuda-toolkit) from NVIDIA; nvprof.  Execute the below cell to profile your task2_out program and we'll see if we can figure out what the problem is.  This is called profiler-driven optimization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "!nvprof ./task2_out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, you can see that with regards to *API calls,* cudaFree and cudaMallocManaged consume the vast majority of time. While unified memory *handles* transfers between the host (CPU) and device (GPU), those tranfers still take time. The *Unified Memory profiling result* shows the total amount of time spent migrating that memory. \n",
    "\n",
    "If your modified [task2.cu](task2-curand/task2.cu) file is similar to the [task2_solution.cu](task2-curand/task2_solution.cu) code, the nvprof will show exactly how much time is spent migrating the data to the device. Here is a clipped output of nvprof for the task 2 solution (exact details may differ depending on your code and which GPU architecture you are using):\n",
    "\n",
    "```\n",
    "==2458== NVPROF is profiling process 2458, command: ./task2_out\n",
    "\n",
    "API calls:\n",
    "81.54%  759.86ms         7  108.55ms  88.919us  360.22ms  cudaFree\n",
    "18.30%  170.55ms         3  56.850ms  26.303us  170.46ms  cudaMallocManaged\n",
    "0.05%   501.29us         2  250.65us  248.55us  252.74us  cuDeviceTotalMem\n",
    "0.04%   419.23us       185  2.2660us     263ns  71.632us  cuDeviceGetAttribute\n",
    "0.03%   296.56us         3  98.852us  11.804us  161.31us  cudaMalloc\n",
    "\n",
    "==2458== Unified Memory profiling result:\n",
    "Device \"Tesla V100-SXM2-16GB (0)\"\n",
    "Count  Avg Size  Min Size  Max Size  Total Size  Total Time  Name\n",
    "8452  46.151KB  4.0000KB  0.9922MB  380.9336MB  57.74902ms  Host To Device\n",
    "742          -         -         -           -  197.7161ms  Gpu page fault groups\n",
    "Total CPU Page faults: 2292\n",
    "```\n",
    "\n",
    "If we can reduce the number of data transfers, we can increase performance. It turns out we have excessive data transfers because we used the host-side version of `curandCreateGeneratorHost`. So what's currently happening is:\n",
    "\n",
    "1. The CPU creates random numbers\n",
    "2. These numbers migrate to the GPU to be used by our `cublasSgemm` call.\n",
    "\n",
    "So we're doing 2 transfers (2x arrays, 1 time each) of data that aren't required!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task #3\n",
    "\n",
    "Your final task is to modify [task3.cu](task3-curand_on_gpu/task3.cu) (the same as [task2_solution.cu](task2-curand/task2_solution.cu) in Task #2) to minimize the number of transfers across the PCI-Express bus.  When complete, make sure to save and then run in the cell below. \n",
    "\n",
    "Make use of the cuRAND [documentation](http://docs.nvidia.com/cuda/curand/host-api-overview.html#generation-functions), the following hint, or the [solution](task3-curand_on_gpu/task3_solution.cu) if needed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id = 'task3'></a>\n",
    "[Hint 1](#task3hint1)    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Execute this cell to compile task3.cu\n",
    "!nvcc -arch=sm_30 -lcublas -lcurand -o task3_out task3/task3.cu && echo Compiled Successfully!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "!./task3_out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In my case, with the modification to the random number generation, our contrived example is now running about three times as fast as we started with - and we did not have to write any GPU specific code, just make a few library API calls.\n",
    "\n",
    "You can now see how leaving data on the GPU for as long as possible, and stringing together API calls from various libraries, can provide great flexibility in creating extremely fast and powerful algorithms!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Learn More\n",
    "\n",
    "If you are interested in learning more, you can use the following resources:\n",
    "\n",
    "* Learn more at the [CUDA Developer Zone](https://developer.nvidia.com/category/zone/cuda-zone).\n",
    "* See a list of available libraries [here](https://developer.nvidia.com/gpu-accelerated-libraries).\n",
    "* If you have an NVIDIA GPU in your system, you can download and install the [CUDA tookit](https://developer.nvidia.com/cuda-toolkit) to access NVIDIA's GPU-accelerated libraries.\n",
    "* Search or ask questions on [Stackoverflow](http://stackoverflow.com/questions/tagged/cuda) using the cuda tag."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"post-lab\"></a>\n",
    "## Post-Lab\n",
    "\n",
    "Finally, don't forget to save your work from this lab before time runs out and the instance shuts down!!\n",
    "\n",
    "1. Save this IPython Notebook by going to `File -> Download as -> IPython (.ipynb)` at the top of this window\n",
    "2. You can execute the following cell block to create a zip-file of the files you've been working on, and download it with the link below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "rm -f library_c_files.zip\n",
    "zip -r library_c_files.zi task*/*.cu task*/*.h"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**After** executing the above cell, you should be able to download the zip file [here](files/library_c_files.zip)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"FAQ\"></a>\n",
    "---\n",
    "# Lab FAQ\n",
    "\n",
    "Q: I'm encountering issues executing the cells, or other technical problems?<br>\n",
    "A: Please see [this](https://developer.nvidia.com/self-paced-labs-faq#Troubleshooting) infrastructure FAQ."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lab Hints"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id = 'task1hint1'></a>\n",
    "\n",
    "## Task 1 - Hint 1\n",
    "\n",
    "The *cublasHandle_t* required by the *cublasSgemm* call has already been created for you in the code.\n",
    "      \n",
    "[Return to task](#task1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id = 'task1hint2'></a>\n",
    "\n",
    "## Task 1 - Hint 2\n",
    "      \n",
    "You will need to use *cudaMallocManaged* to  allocate space for the *a*, *b*, and *c* arrays in unified memory.  These pointers have already been declared for you.\n",
    "      \n",
    "[Return to task](#task1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id = 'task1hint3'></a>\n",
    "\n",
    "## Task 1 - Hint 3\n",
    "      \n",
    "You'll need to use the [cublasSgemm documentation](http://docs.nvidia.com/cuda/cublas/index.html#cublas-lt-t-gt-gemm) to determine the remaining parameters needed in the call.\n",
    "      \n",
    "[Return to task](#task1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='task2hint1'></a>\n",
    "\n",
    "## Task 2 - Hint 1\n",
    "\n",
    "For this task, make sure to use the Host version of the *curandCreateGeneratorHost* call.  Well explore the device-side version in the next task.\n",
    "\n",
    "[Return to task](#task2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='task2hint2'></a>\n",
    "\n",
    "## Task 2 - Hint 2\n",
    "There's a great example showing how to use the cuRAND library located [here](http://docs.nvidia.com/cuda/curand/host-api-overview.html#host-api-example).\n",
    "\n",
    "[Return to task](#task2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id = 'task3hint1'></a>\n",
    "\n",
    "## Task 3 - Hint 1\n",
    "\n",
    "To have the *cuRAND* library fill a device-side array, you'll want to use *curandCreateGenerator* without the *Host* on the end.\n",
    "\n",
    "[Return to task](#task3)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
